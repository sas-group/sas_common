use std::error::Error;

#[derive(Debug)]
pub struct SASError {
    code: u32,
    message: String,

    inner_error: Option<Box<dyn Error>>,
}

impl SASError {
    pub fn new(errcode: u32) -> SASError {
        SASError {
            code: errcode,
            inner_error: None,
            message: String::new(),
        }
    }

    pub fn from_error(error: impl Error + 'static) -> SASError {
        SASError {
            code: 1,
            message: String::new(),
            inner_error: Some(Box::new(error)),
        }
    }
}

impl From<std::string::String> for SASError {
    fn from(s: std::string::String) -> Self {
        return SASError {
            code: 1,
            message: s,
            inner_error: None
        }
    }
}

impl From<&str> for SASError {
    fn from(s: &str) -> Self {
        return SASError {
            code: 1,
            message: s.to_string(),
            inner_error: None
        }
    }
}

impl From<std::string::FromUtf8Error> for SASError {
    fn from(e: std::string::FromUtf8Error) -> Self {
        return SASError {
            code: 1,
            message: "UTF8 Error".into(),
            inner_error: Some(Box::new(e)),
        };
    }
}

impl From<std::num::ParseIntError> for SASError {
    fn from(e: std::num::ParseIntError) -> Self {
        return SASError {
            code: 1,
            message: "Parsing Error".into(),
            inner_error: Some(Box::new(e)),
        };
    }
}

impl From<mysql::error::UrlError> for SASError {
    fn from(e: mysql::error::UrlError) -> Self {
        return SASError {
            code: 1,
            message: "MySQL Error".into(),
            inner_error: Some(Box::new(e)),
        };
    }
}

impl From<mysql::error::Error> for SASError {
    fn from(e: mysql::error::Error) -> Self {
        return SASError {
            code: 1,
            message: "MySQL Error".into(),
            inner_error: Some(Box::new(e)),
        };
    }
}

impl From<std::io::Error> for SASError {
    fn from(e: std::io::Error) -> Self {
        return SASError {
            code: 1,
            message: "IO Error".into(),
            inner_error: Some(Box::new(e)),
        };
    }
}

// impl From<dyn Error> for SASError {
//     fn from(e: dyn Error) -> Self {
//         todo!()
//     }
// }

impl std::error::Error for SASError {
    //
}

impl std::fmt::Display for SASError {
    fn fmt(
        &self,
        formatter: &mut std::fmt::Formatter<'_>,
    ) -> std::result::Result<(), std::fmt::Error> {
        let data = format!("SAS ERROR {}.", self.code);
        formatter.write_str(&data)?;

        return Ok(());
    }
}
