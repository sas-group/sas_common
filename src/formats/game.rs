use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub enum StorageType {
    String,
    Number,
    Boolean,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct EventData {
    pub data: Vec<u8>,
    pub typ: StorageType,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct GameEvent {
    pub index: usize,
    pub data: Vec<EventData>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct SimulationRun {
    pub id: Uuid,
    pub events: Vec<GameEvent>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct GameType {
    pub name: String,
    pub runs: Vec<SimulationRun>,
}
