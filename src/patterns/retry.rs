use std::future::Future;

pub struct Retry {
    count: usize,
    waittime: Option<std::time::Duration>,
}

impl Retry {
    pub fn new() -> Retry {
        Retry {
            count: 0,
            waittime: None,
        }
    }

    pub fn max_tries(mut self, count: usize) -> Retry {
        self.count = count;

        return self;
    }

    pub fn infinite(mut self) -> Retry {
        self.count = 0;

        return self;
    }

    pub fn wait(mut self, time: std::time::Duration) -> Retry {
        self.waittime = Some(time);

        return self;
    }

    pub fn run<T, E, F>(self, mut func: impl FnMut() -> Result<T, E>) -> Result<T, RetryError>
    where
        E: std::fmt::Debug,
    {
        let mut current: usize = 0;

        loop {
            let res: Result<T, E> = func();

            if res.is_ok() {
                return Ok(res.unwrap());
            } else {
                if self.count == 0 {
                    if let Some(wait) = self.waittime.as_ref() {
                        std::thread::sleep(*wait);
                    } else {
                        std::thread::yield_now();
                    }

                    continue;
                } else {
                    current += 1;

                    if current > self.count {
                        return Err(RetryError::Timeout);
                    }
                }
            }
        }
    }

    pub async fn run_async<T, E, F>(self, mut func: impl FnMut() -> F) -> Result<T, RetryError>
    where
        F: Future<Output = Result<T, E>>,
        E: std::fmt::Debug,
    {
        let mut current: usize = 0;

        loop {
            let res: Result<T, E> = func().await;

            if res.is_ok() {
                return Ok(res.unwrap());
            } else {
                if self.count == 0 {
                    if let Some(wait) = self.waittime.as_ref() {
                        tokio::time::delay_for(*wait).await;
                    } else {
                        tokio::task::yield_now().await;
                    }

                    continue;
                } else {
                    current += 1;

                    if current > self.count {
                        return Err(RetryError::Timeout);
                    }
                }
            }
        }
    }
}

#[derive(std::fmt::Debug)]
pub enum RetryError {
    Timeout,
    Inner(Box<dyn std::error::Error>)
}
