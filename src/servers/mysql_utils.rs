
pub async fn await_connection(opts: &mysql::Opts) -> mysql::Conn {
    loop {
        let res = mysql::Conn::new(opts.clone());

        if let Ok(conn) = res {
            return conn;
        }

        tokio::task::yield_now().await;
    }
}
