use super::mysql_utils::*;
use crate::error::*;
use crate::time::stopwatch::*;
use mysql::prelude::*;

use std::{
    io::{BufRead, BufReader, BufWriter, Write},
    net::*,
};

pub struct PreStoreServer {
    server: TcpListener,
    admin_connection: mysql::Conn,
    db_connstring: String,
}

impl PreStoreServer {
    pub fn connect(address: &str, db_str: &str) -> std::result::Result<PreStoreServer, SASError> {
        // Connect to the Database server. Return error if no
        let db_addr = mysql::Opts::from_url(&db_str)?;
        let db = mysql::Conn::new(db_addr)?;

        let local_tcp = TcpListener::bind(address)?;

        Ok(PreStoreServer {
            server: local_tcp,
            db_connstring: db_str.to_string(),
            admin_connection: db,
        })
    }

    pub fn run(&mut self) -> Result<(), SASError> {
        println!("Server started successfully. Waiting for clients.");

        let rt = tokio::runtime::Builder::new()
            .threaded_scheduler()
            .enable_all()
            .build()
            .expect("Unable to build a tokio runtime.");

        let my_iter = self.server.incoming().filter_map(|e| e.ok());

        for i in my_iter {
            let db_conn = self.db_connstring.clone();

            let client_addr = i.peer_addr()?;

            println!("Client connected from {}.", client_addr);

            rt.spawn(async {
                PreStoreServer::handle_client(i, db_conn)
                    .await
                    .expect("Should work.");
            });
        }

        return Ok(());
    }

    async fn handle_client(
        client: TcpStream,
        db_str: String,
    ) -> std::result::Result<usize, SASError> {
        let mut reader = BufReader::new(&client);
        let mut writer = BufWriter::new(&client);
        let mut sw = Stopwatch::new();

        // Receive game name
        // Receive typestring
        // Receive sim uid
        // Receive each csv of the games
        let mut bufgname = Vec::new();
        reader.read_until(b'|', &mut bufgname)?;

        let mut bufhead = Vec::new();
        reader.read_until(b'|', &mut bufhead)?;

        let mut bufts = Vec::new();
        reader.read_until(b'|', &mut bufts)?;

        let mut bufuid = Vec::new();
        reader.read_until(b'|', &mut bufuid)?;

        let gamename = String::from_utf8(bufgname)?;
        let headers = String::from_utf8(bufhead)?;
        let typestring = String::from_utf8(bufts)?;
        let uuid = String::from_utf8(bufuid)?;

        let gamename = gamename.trim_end_matches('|');
        let headers = headers.trim_end_matches('|');
        let typestring = typestring.trim_end_matches('|');
        let uuid = uuid.trim_end_matches('|');

        let db_addr = mysql::Opts::from_url(&db_str)?;
        let mut db = mysql::Conn::new(db_addr)?;

        println!("Connected to database at {}.", db_str);

        let mut count = 0;

        let mut games = Vec::new();

        loop {
            let mut buff = Vec::new();
            let bytes_read = reader.read_until(b'|', &mut buff)?;

            if bytes_read == 0 {
                break;
            }

            let strbuf = String::from_utf8(buff)?;
            let strbuf = strbuf.trim_end_matches('|');

            games.push(strbuf.to_owned());

            if games.len() == 5000 {
                let gamecpy = games.clone();
                let gamename = gamename.to_owned();
                let headers = headers.to_owned();
                let typestring = typestring.to_owned();
                let uuid = uuid.to_owned();

                let db_addr = mysql::Opts::from_url(&db_str)?;
                let mut db = await_connection(&db_addr).await;

                tokio::spawn(async move {
                    store_games(&mut db, &gamename, &headers, &typestring, &uuid, &gamecpy).unwrap();
                });

                games.clear();
            }

            count += 1;
        }

        if games.len() > 0 {
            store_games(&mut db, &gamename, &headers, &typestring, &uuid, &games)?;

            games.clear();
        }

        writer.write_all(b"Bye")?;

        let duration = sw.stop();

        println!(
            "Connected with {} ended. Inserted {} items in {}ms.",
            client.peer_addr().unwrap(),
            count,
            duration.as_millis()
        );

        return Ok(count);
    }
}

fn store_games(
    db: &mut mysql::Conn,
    gamename: &str,
    headers: &str,
    typestring: &str,
    uuid: &str,
    games: &Vec<String>,
) -> Result<usize, SASError> {
    let mut cmd = String::new();

    cmd.push_str("INSERT INTO preproc (gamename, headers, typestring, uid, data) VALUES");

    for (index, g) in games.iter().enumerate() {
        let value = format!(
            "(\"{}\", \"{}\", \"{}\", \"{}\", \"{}\")",
            gamename, headers, typestring, uuid, g
        );

        cmd.push_str(&value);

        if index != (games.len() - 1) {
            cmd.push_str(",");
        }
    }

    db.query_drop(cmd)?;

    // Not really a correct count of added elements.
    return Ok(games.len());
}

pub struct PreStoreClient {
    server: TcpStream,
}

impl PreStoreClient {
    pub fn connect(address: &str) -> std::result::Result<PreStoreClient, SASError> {
        Ok(PreStoreClient {
            server: TcpStream::connect(address)?,
        })
    }

    pub fn setup(&mut self, gamename: &str, headers: &str, typestring: &str, uid: &str) -> Result<(), SASError> {
        self.server.write_all(gamename.as_bytes())?;
        self.server.write(b"|")?;

        self.server.write_all(headers.as_bytes())?;
        self.server.write(b"|")?;

        self.server.write_all(typestring.as_bytes())?;
        self.server.write(b"|")?;

        self.server.write_all(uid.as_bytes())?;
        self.server.write(b"|")?;

        return Ok(());
    }

    pub fn send_data(&mut self, data: &str) -> Result<usize, SASError> {
        self.server.write_all(data.as_bytes())?;
        let res = self.server.write(b"|")?;

        return Ok(res);
    }
}

/**
 * Secondary struct used to communicate to the PreStore DB directly instead
 * of going through a PreStore server. This is because I'm reviewing the use
 * of the PreStore server. It's not really serving any RPC calls so "service"
 * methods can't be called on it. In some cases we really just want to update
 * some rows on the database and we need an abstraction to allow us to do this.
 */
pub struct PreStoreDatabase {
    conn: mysql::Conn
}

impl PreStoreDatabase {
    pub fn connect(addr: &str) -> Result<PreStoreDatabase, SASError> {
        let opt = mysql::Opts::from_url(addr)?;
        let db = mysql::Conn::new(opt)?;

        return Ok(PreStoreDatabase {
            conn: db
        });
    }

    pub fn with_connection(connection: mysql::Conn) -> PreStoreDatabase {
        return PreStoreDatabase {
            conn: connection
        }
    }

    /**
     * Lock a simulation for exclusive processing.
     *
     * This is used by the Logical Storage Processors to request exclusive
     * access to a simulation's events.
     *
     * There is a Unique index on the `simUid` column so that no two processors
     * can lock the same row at the same time. If this method returns an error
     * the caller should try another simulation.`
     */
    pub fn lock_sim(&mut self, uid: &uuid::Uuid) -> Result<(), SASError> {
        let str_uid = uid.to_hyphenated();

        let cmd = format!("INSERT INTO ProcessLock (simUid) VALUES ({});", str_uid);

        return self.conn.query_drop(cmd).map_err(|e| e.into());
    }

    pub fn unlock_sim(&mut self, uid: &uuid::Uuid) -> Result<(), SASError> {
        let str_uid = uid.to_hyphenated();

        let cmd = format!("DELETE FROM ProcessLock WHERE simUid = {};", str_uid);

        self.conn.query_drop(cmd)?;

        let affected = self.conn.affected_rows();

        if affected != 1 {
            return Err(SASError::new(1));
        }

        return Ok(());
    }

    /**
     * Get a list of simulation UID's that are on the server.
     *
     * Any simulation that are found are to be processed by the logical storage
     * servers.
     */
    pub fn get_sim_ids(&mut self) -> Result<Vec<uuid::Uuid>, SASError> {
        let cmd = "SELECT DISTINCT uid FROM `preproc`";

        let res = self.conn.query_map(cmd, |uid| {
            let data: String = uid;

            return uuid::Uuid::parse_str(&data).unwrap();
        }).map_err(|e| e.into());

        return res;
    }

    /**
     * Get a list of all simulations that are not locked in the lock table.
     */
    pub fn get_free_sim_uids(&mut self) -> Result<Vec<uuid::Uuid>, SASError> {
        let cmd = "SELECT DISTINCT preproc.uid FROM preproc
        LEFT JOIN ProcessLock ON preproc.uid = ProcessLock.uid
        WHERE ProcessLock.uid IS NULL;";

        let res = self.conn.query_map(cmd, |uid| {
            let data: String = uid;

            return uuid::Uuid::parse_str(&data).unwrap();
        }).map_err(|e| e.into());

        return res;
    }

    /**
     * Combines scanning for free sim and locking it.
     *
     * This funtion is getting a list of free sims and tries to lock one of
     * them. It returns as soon as a lock is applied.
     *
     * We need this method to make sure that 2 servers can't lock a sim
     * at the same time. Because the lock table has a unique index on its ID,
     * if a second server tries to double-lock it, it will receive an error and
     * can try with another row in the free list.
     */
    pub fn find_and_lock_sim(&mut self) -> Result<Option<uuid::Uuid>, SASError> {
        let free_sims = self.get_free_sim_uids()?;

        if free_sims.is_empty() {
            return Ok(None);
        }

        for uid in free_sims {
            // TODO : Determine if true error or just sim already locked.
            let res = self.lock_sim(&uid);

            if res.is_ok() {
                return Ok(Some(uid));
            }
        }

        return Ok(None);
    }
}