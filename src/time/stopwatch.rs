pub struct Stopwatch {
    start: std::time::Instant,
    end: Option<std::time::Instant>
}

impl Stopwatch {
    pub fn new() -> Stopwatch {
        Stopwatch {
            start: std::time::Instant::now(),
            end: None
        }
    }

    pub fn stop(&mut self) -> std::time::Duration {
        if let Some(end) = self.end {
            return end.duration_since(self.start);
        }

        let end = std::time::Instant::now();
        let total = end.duration_since(self.start);

        self.end = Some(end);

        return total;
    }
}