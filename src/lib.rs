pub mod config;
pub mod error;
pub mod formats;
pub mod servers;
pub mod time;
pub mod patterns;

#[cfg(test)]
mod tests {
    use crate::servers::pre_store::*;
    use uuid::*;

    #[test]
    #[ignore]
    fn server_test() {
        let server_str = "127.0.0.1:4545";
        let db_str = "mysql://root:abcd1234@localhost:3306/pre_storage";

        let pserv = PreStoreServer::connect(server_str, db_str);

        if let Ok(mut server) = pserv {
            server.run().expect("Server runtime error");
        } else {
            println!("Unable to bind to local port 4545.");
        }
    }

    #[test]
    #[ignore]
    fn server_retry_test() {
        let server_str = "127.0.0.1:4545";
        let db_str = "mysql://root:abcd1234@localhost:3306/pre_storage";

        let mut server = loop {
            let conn_test = PreStoreServer::connect(server_str, db_str);

            if let Ok(server) = conn_test {
                break server;
            } else {
                println!("Server setup failed. Retrying in 5 sec.");
                std::thread::sleep(std::time::Duration::from_secs(5));
            }
        };

        loop {
            let run_res = server.run();

            if let Err(e) = run_res {
                println!("Error {} when running server. Retrying.", e);

                std::thread::sleep(std::time::Duration::from_secs(1));
            } else {
                break;
            }
        };

        println!("Execution finished.");
    }

    #[test]
    #[ignore]
    fn client_test() {
        let mut client = PreStoreClient::connect("localhost:4545")
            .expect("Expected to connect to remote server.");

        client.setup(
            "testgame",
            "one, two, three",
            "number, number, number",
            &Uuid::new_v4().to_string(),
        ).expect("General error setting up client.");

        for _ in 0..1000000 {
            client.send_data("1, 2, 3").expect("Should work");
        }
    }
}
