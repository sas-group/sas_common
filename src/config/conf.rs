use etcd_rs::*;
use std::convert::From;
use std::fs::*;
use std::string::String;
use std::sync::*;
use tokio::runtime;
use hashbrown::HashMap;

pub struct Config {
    env_prefix: Option<String>,
    file: Option<File>,
    etcd_server: Option<Client>,
    local_values: HashMap<String, String>,

    parent: Weak<Config>,
}

impl Config {
    pub fn new() -> Config {
        return Config {
            env_prefix: None,
            file: None,
            etcd_server: None,
            local_values: HashMap::new(),
            parent: Weak::default(),
        };
    }

    pub fn with_env_prefix(mut self, prefix: &str) -> Self {
        self.env_prefix = Some(prefix.into());

        return self;
    }

    pub fn with_file(mut self, path: &str) -> Self {
        if let Ok(file) = File::open(path) {
            self.file = Some(file);
        }

        return self;
    }

    pub fn with_etcd(mut self, address: Vec<String>) -> Self {
        let mut basic_rt = runtime::Builder::new()
            .basic_scheduler()
            .enable_all()
            .build()
            .unwrap();

        let client = basic_rt.block_on(async {
            let client = Client::connect(ClientConfig {
                endpoints: address,
                auth: None,
            })
            .await
            .unwrap();

            return client;
        });

        self.etcd_server = Some(client);

        return self;
    }

    pub fn get_any<T>(_name: &str) -> Option<T> {
        return None;
    }

    pub fn get<T: From<String>>(&self, name: &str) -> Option<T> {
        // Local values will always take precedence.
        if let Some(value) = self.local_values.get(name) {
            return Some(value.to_owned().into());
        }

        // Look in the current environement variables.
        if let Some(prefix) = self.env_prefix.as_ref() {
            let var_name = format!("{}_{}", prefix, name);

            if let Ok(res) = std::env::var(var_name) {
                return Some(res.into());
            }
        }

        // Look in a local config file.
        if let Some(_file) = self.file.as_ref() {
            // TODO : Check in file
        };

        // Check in the etcd cluster.
        if let Some(etcd) = self.etcd_server.as_ref() {
            // TODO : Save runtime
            let mut basic_rt = runtime::Builder::new()
                .basic_scheduler()
                .enable_all()
                .build()
                .unwrap();

            let res = basic_rt.block_on(async {
                let mut kv = etcd.kv();
                let res = kv.range(RangeRequest::new(KeyRange::key(name))).await;

                if let Ok(mut found) = res {
                    return Some(found.take_kvs().first().unwrap().value().to_vec());
                }

                return None;
            });

            if let Some(found) = res {
                return Some(String::from_utf8(found).unwrap().into());
            }
        }

        // If none was found, check in parent.
        if let Some(parent) = self.parent.upgrade() {
            return parent.get(name);
        }

        return None;
    }

    pub fn add(&mut self, name: String, value: String) {
        self.local_values.insert(name, value);
    }

    pub fn set_parent(&mut self, parent: Arc<Self>) {
        self.parent = Arc::downgrade(&parent);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn config_test_empty() {
        let conf = Config::new();

        // Test value not found
        let result: Option<String> = conf.get("TEST");
        assert_eq!(result, None);

        // Test empty name
        let result: Option<String> = conf.get("");
        assert_eq!(result, None);
    }

    #[test]
    fn config_test_local_values() {
        let mut conf = Config::new();

        let previous: Option<String> = conf.get("TEST_VALUE");
        assert_eq!(previous, None);

        conf.add("TEST_VALUE".to_string(), "abcde".to_string());

        let new: Option<String> = conf.get("TEST_VALUE");
        assert_eq!(new, Some("abcde".to_string()));

        conf.add("TEST_VALUE".to_string(), "xyz".to_string());

        let changed: Option<String> = conf.get("TEST_VALUE");
        assert_eq!(changed, Some("xyz".to_string()));
    }

    #[test]
    fn config_test_env() {
        let mut conf = Config::new().with_env_prefix("APP");

        let previous: Option<String> = conf.get("TEST_VALUE");
        assert_eq!(previous, None);

        std::env::set_var("APP_TEST_VALUE", "12345");

        let current: Option<String> = conf.get("TEST_VALUE");
        assert_eq!(current, Some("12345".to_owned()));

        // Local values override environement
        conf.add("TEST_VALUE".to_owned(), "qazwsxedc".to_owned());

        let overridden: Option<String> = conf.get("TEST_VALUE");
        assert_eq!(overridden, Some("qazwsxedc".to_owned()));
    }

    #[test]
    fn config_tests() {
        std::env::set_var("APP_MYTEST", "aaa");

        let conf = Config::new().with_env_prefix("APP");

        let res = conf.get::<String>("MYTEST");
        assert!(res == Some("aaa".to_string()));

        let res = conf.get("NotFound").unwrap_or("385".to_string());
        assert!(res == "385");

        let res: u32 = res.parse().unwrap();

        assert!(res == 385);
    }

    #[test]
    #[ignore]
    fn etcd_tests() {
        let conf = Config::new().with_etcd(["http://localhost:2379".to_string()].to_vec());

        let _ingest_addr = conf
            .get::<String>("INGEST_ADDR")
            .expect("Env var INGEST_ADDR not found.");
    }
}
