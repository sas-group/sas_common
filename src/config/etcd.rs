use crate::error::*;
use etcd_rs::*;

pub async fn try_fetch_value() -> std::result::Result<(), SASError> {
    let endpoints = vec!["http://127.0.0.1:2379".to_owned()];

    let client = Client::connect(ClientConfig {
        endpoints,
        auth: None,
    })
    .await
    .unwrap();

    let resp = client
        .kv()
        .put(PutRequest::new("aaa", "abcd"))
        .await
        .unwrap();

    println!("Put Response: {:?}", resp);

    // Get the key-value pair
    let resp = client
        .kv()
        .range(RangeRequest::new(KeyRange::key("aaa")))
        .await
        .unwrap();
    println!("Range Response: {:?}", resp);

    return Ok(());
}

#[cfg(test)]
mod tests {
    use super::*;
    use tokio::runtime;

    #[test]
    #[ignore]
    fn do_stuff() {
        let mut basic_rt = runtime::Builder::new()
            .basic_scheduler()
            .enable_all()
            .build()
            .unwrap();

        basic_rt.block_on(try_fetch_value()).unwrap();
    }
}
